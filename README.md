OBSD_maker
==========

Usage
-----

Just run

    make ARCH=<your architecture, ie "amd64>

To build a `RetroBSD.iso` file.


Configuration
-------------

see [obsdçiso_maker](https://framagit.org/Thuban/obsd_iso_maker)
